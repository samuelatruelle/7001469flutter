import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: "Starter Template",
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: Home(),
    );
  }
}

class Home extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.amber,
        title: new Text("AppBar"),
        actions: <Widget>[
          IconButton(
              icon: Icon(Icons.add),
              onPressed: () {
                print("Add IconButton Pressed...");
              })
        ],
      ),
      body: new Center(
        child: new Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[new Text("Body")],
        ),
      ),
      drawer: Drawer(
          child: ListView(children: <Widget>[
        Row(children: <Widget>[
          IconButton(
              icon: Icon(Icons.add),
              onPressed: () {
                print("Drawer Item 1 Pressed");
              }),
          Text("Drawer Item 1")
        ]),
        Row(children: <Widget>[
          IconButton(
              icon: Icon(Icons.add),
              onPressed: () {
                print("Drawer Item 2 Pressed");
              }),
          Text("Drawer Item 2")
        ])
      ])),
      bottomSheet: Container(
          color: Colors.amberAccent,
          padding: EdgeInsets.all(20.0),
          child: Row(children: <Widget>[
            IconButton(
                icon: Icon(Icons.update),
                onPressed: () {
                  print("Bottom Sheet Icon Pressed");
                }),
            Text("Bottom Sheet Text")
          ])),
      persistentFooterButtons: <Widget>[
        IconButton(
            icon: Icon(Icons.update),
            onPressed: () {
              print("Persistant Footer Icon Pressed");
            }),
        Text("Persistant Footer Text")
      ],
      floatingActionButton: new FloatingActionButton(
          onPressed: () {
            print("FloatingActionButton Pressed");
          },
          tooltip: "Increment",
          child: new Icon(Icons.add),
        backgroundColor: Colors.lightGreen,
      ),
      bottomNavigationBar: BottomNavigationBar(
        type: BottomNavigationBarType.fixed,
        onTap: (index) => debugPrint("Bottom Navigation Bar onTap: $index"),
        items: [
          BottomNavigationBarItem(
            icon: new Icon(Icons.home),
            title: new Text('Bottom Nav Bar Item 1'),
          ),
          BottomNavigationBarItem(
            icon: new Icon(Icons.mail),
            title: new Text("Bottom Nav Bar Item 2"),
          )
        ],
      ),
    );
  }
}
