import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: "Starter Template",
      theme: ThemeData(
        primarySwatch: Colors.lightGreen,
      ),
      home: Home(),
    );
  }
}

class Home extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Template"),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            const CenterCardPadding(),
            const ExpandedFlexiblePositionedWidget()
          ],
        ),
      )
    );
  }
}

class CenterCardPadding extends StatelessWidget {
  const CenterCardPadding({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
        child: const Card(
      color: Colors.lightGreen,
      child: Padding(
        padding: EdgeInsets.all(100.0),
        child: Text(
          'Hello World!',
          style: TextStyle(fontSize: 30),
        ),
      ),
    ));
  }
}

class ExpandedFlexiblePositionedWidget extends StatelessWidget {
  const ExpandedFlexiblePositionedWidget({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        width: double.infinity,
        height: 500.0,
        child: Stack(children: <Widget>[
          Column(children: <Widget>[
            Row(
              children: <Widget>[
                Expanded(
                  child: RaisedButton(
                    onPressed: () {},
                    child: Text("Expand"),
                  ),
                ),
                RaisedButton(
                  onPressed: () {},
                  child: Text("Not Expand"),
                  color: Colors.lightGreen,
                )
              ],
            ),
            Row(
              children: <Widget>[
                Expanded(
                  child: RaisedButton(
                    onPressed: () {},
                    child: Text("Expand"),
                  ),
                ),
                Expanded(
                  child: RaisedButton(
                    onPressed: () {},
                    child: Text("Expand"),
                  ),
                ),
              ],
            ),
            Row(
              children: <Widget>[
                Flexible(
                  fit: FlexFit.tight,
                  child: RaisedButton(
                    onPressed: () {},
                    child: Text("FlexFit.tight"),
                  ),
                ),
                Flexible(
                  fit: FlexFit.loose,
                  child: RaisedButton(
                    onPressed: () {},
                    child: Text("FlexFit.tight"),
                  ),
                )
              ],
            ),
          ]),
          Positioned(
            left: 30,
            top: 150,
            child: Container(
              child: Row(
                children: <Widget>[
                  Expanded(
                    child: RaisedButton(
                      onPressed: () {},
                      child: Text("Expand"),
                    ),
                  ),
                  RaisedButton(
                    onPressed: () {},
                    child: Text("Not Expand"),
                    color: Colors.lightGreen,
                  )
                ],
              ),
              color: Colors.lightGreen,
              height: 150,
              width: 150,

            ),
          )
        ]));
  }
}
