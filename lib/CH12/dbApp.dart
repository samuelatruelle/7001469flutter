import 'package:flutter/material.dart';
import 'package:sqflite/sqflite.dart';
import 'dart:io' as io;
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: "Starter Template",
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: Home(),
    );
  }
}

class Home extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        primarySwatch: Colors.lightGreen,
      ),
      home: StudentPage(),
    );
  }
}

class StudentPage extends StatefulWidget {
      @override
    _StudentPageState createState() => _StudentPageState();
}

class _StudentPageState extends State<StudentPage> {
  final GlobalKey<FormState> _formStateKey = GlobalKey<FormState>();
  Future<List<Student>> students;
  int studentIdForUpdate;
  String _studentName;
  bool isUpdate = false;
  DBHelper dbHelper;
  final _studentNameController = TextEditingController();

  @override
  void initState() {
    super.initState();
    dbHelper = DBHelper();
    refreshStudentList();
  }

  refreshStudentList() {
    setState(() {
      students = dbHelper.getStudents();
    });
  }

  SingleChildScrollView generateList(List<Student> students) {
    return SingleChildScrollView(
      scrollDirection: Axis.vertical,
      child: SizedBox(
        width: double.infinity,
        child: DataTable(
          columns: [
            DataColumn(
                label: Text('NAME')
            ),
            DataColumn(
              label: Text('DELETE'),
            )
          ],
          rows: students.map((student) =>
              DataRow(
                cells: [
                  DataCell(
                    Text(student.name),
                    onTap: () {
                      setState(() {
                        isUpdate = true;
                        studentIdForUpdate = student.id;
                      });
                      _studentNameController.text = student.name;
                    },
                  ),
                  DataCell(
                    IconButton(
                      icon: Icon(Icons.delete),
                      onPressed: () {
                        dbHelper.delete(student.id);
                        refreshStudentList();
                      },
                    ),
                  )
                ],
              ),
          ).toList(),),),);
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return
      Scaffold(
          appBar: AppBar(
            title: Text('SQLite CRUD'),
          ),
          body: Column(
              children: <Widget>[
                Form(
                  key: _formStateKey,
                  autovalidate: true,
                  child: Padding(
                    padding: EdgeInsets.only(left: 10, right: 10, bottom: 10),
                    child: TextFormField(
                      validator: (value) =>
                      value.isEmpty
                          ? 'Please Enter Student Name'
                          : null,
                      onSaved: (value) => _studentName = value,
                      controller: _studentNameController,
                      decoration: InputDecoration(
                          labelText: "Student Name",
                          labelStyle: TextStyle(
                            color: Colors.lightGreen,
                          )),
                    ),
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    RaisedButton(
                      color: Colors.lightGreen,
                      child: Text(
                        (isUpdate ? 'UPDATE' : 'ADD'),
                        style: TextStyle(color: Colors.white),
                      ),
                      onPressed: () {
                        if (isUpdate) {
                          if (_formStateKey.currentState.validate()) {
                            _formStateKey.currentState.save();
                            dbHelper
                                .update(
                                Student(studentIdForUpdate, _studentName))
                                .then((data) {
                              setState(() {
                                isUpdate = false;
                              });
                            });
                          }
                        } else {
                          if (_formStateKey.currentState.validate()) {
                            _formStateKey.currentState.save();
                            dbHelper.add(Student(null, _studentName));
                          }
                        }
                        _studentNameController.text = '';
                        refreshStudentList();
                      },
                    ),
                    Padding(
                      padding: EdgeInsets.all(10),
                    ),
                    RaisedButton(
                      color: Colors.red,
                      child: Text(
                        ("CANCEL"),
                        style: TextStyle(color: Colors.white),
                      ),
                      onPressed: () {
                        _studentNameController.text = '';
                        setState(() {
                          //isUpdate = false;
                          studentIdForUpdate = null;
                        });
                      },),
                  ],
                ),


                const Divider(
                  height: 5.0,
                ),
                Expanded(
                  child: FutureBuilder(
                    future: students,
                    builder: (context, snapshot) {
                      if (snapshot.hasData) {
                        return generateList(snapshot.data);
                      }
                      if (snapshot.data == null || snapshot.data.length == 0) {
                        return Text('No Data Found');
                      }
                      return CircularProgressIndicator();
                    },
                  ),
                )
              ]
          )
      );
  }

}

class Student {
  int id;
  String name;

  Student(this.id, this.name);

  Map<String, dynamic> toMap() {
    var map = <String, dynamic>{
      'id': id,
      'name': name,
    };
    return map;
  }

  Student.fromMap(Map<String, dynamic> map) {
    id = map['id'];
    name = map['name'];
  }
}

class DBHelper {
  static Database _db;

  Future<Database> get db async {
    if (_db != null) {
      return _db;
    }
    _db = await initDatabase();
    return _db;
  }

  initDatabase() async {
    io.Directory documentDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentDirectory.path, 'student.db');
    var db = await openDatabase(path, version: 1, onCreate: _onCreate);
    return db;
  }

  _onCreate(Database db, int version) async {
    await db
        .execute('CREATE TABLE student (id INTEGER PRIMARY KEY, name TEXT)');
  }

  Future<Student> add(Student student) async {
    var dbClient = await db;
    student.id = await dbClient.insert('student', student.toMap());
    return student;
  }

  Future<List<Student>> getStudents() async {
    var dbClient = await db;
    List<Map> maps = await dbClient.query('student', columns: ['id', 'name']);
    List<Student> students = [];
    if (maps.length > 0) {
      for (int i = 0; i < maps.length; i++) {
        students.add(Student.fromMap(maps[i]));
      }
    }
    return students;
  }

  Future<int> delete(int id) async {
    var dbClient = await db;
    return await dbClient.delete(
      'student',
      where: 'id = ?',
      whereArgs: [id],
    );
  }

  Future<int> update(Student student) async {
    var dbClient = await db;
    return await dbClient.update(
      'student',
      student.toMap(),
      where: 'id = ?',
      whereArgs: [student.id],
    );
  }

  Future close() async {
    var dbClient = await db;
    dbClient.close();
  }

}

