import 'package:flutter/material.dart';

void main() => runApp(NavigatorApp());

class NavigatorApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: "Navigator",
      theme: ThemeData(
        primarySwatch: Colors.lightGreen,
      ),
      home: NavigatorHomePage(),
    );
  }
}

class NavigatorHomePage extends StatefulWidget {
  NavigatorHomePage({Key key}):super(key: key);

  @override
  _NavigatorPageState createState() => _NavigatorPageState();
}

class _NavigatorPageState extends State<NavigatorHomePage> {
  String _howAreYou = "...";

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.info),
            onPressed: () =>
                _openPageGratitude(
                  context: context,
                  fullscreenDialog: true,
                ),
          )
        ],
      ),
      body: SafeArea(
        child: Padding(
          padding: EdgeInsets.all(16.0),
          child: Text(
            'Grateful for: $_howAreYou', style: TextStyle(fontSize: 32.0),
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () => _openPageGratitude(
            context: context,
            fullscreenDialog: false
        ),
        tooltip: 'About',
        child: Icon(Icons.sentiment_satisfied),
      ),
    );
  }

  void _openPageGratitude(
      {BuildContext context, bool fullscreenDialog = false}) async {
    final String _gratitudeResponse = await Navigator.push(
      context,
      MaterialPageRoute(
        fullscreenDialog: fullscreenDialog,
        builder: (context) => Gratitude(),
      )
    );
    _howAreYou = _gratitudeResponse;
  }
}

class About extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("About"),
        ),
        body: Card(
          child: Center(
            child: Text("This is about page"),
          ),
        )
    );
  }
}

class Gratitude extends StatefulWidget {
  Gratitude({Key key}) : super(key: key);

  @override
  _GratitudeState createState() => _GratitudeState();
}

class _GratitudeState extends State<Gratitude> {

  int _radioGroupValue;
  List<String> _gratitudeList = List();
  String _selectedGratitude;

  void _radioOnChanged(int index) {
    setState(() {
      _radioGroupValue = index;
      _selectedGratitude = _gratitudeList[index];
      print('_selectedRadioValue $_selectedGratitude');
    });
  }

  @override
  void initState() {
    super.initState();
    _gratitudeList..add('Family')..add('Friends')..add('Coffee');
    _radioGroupValue = 0;
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
        appBar: AppBar(
          title: Text('Gratitude'),
          actions: <Widget>[
            IconButton(
              icon: Icon(Icons.check),
              onPressed: () => Navigator.pop(context, _selectedGratitude),
            ),
          ],
        ),
        body: SafeArea(
          child: Padding(
            padding: const EdgeInsets.all(16.0),
            child: Row(
              children: <Widget>[
                Radio(
                  value: 0,
                  groupValue: _radioGroupValue,
                  onChanged: (index) => _radioOnChanged(index),
                ),
                Text('Family'),
                Radio(
                  value: 1,
                  groupValue: _radioGroupValue,
                  onChanged: (index) => _radioOnChanged(index),
                ),
                Text('Friends'),
                Radio(
                  value: 2,
                  groupValue: _radioGroupValue,
                  onChanged: (index) => _radioOnChanged(index),

                ),
                Text('Coffee'),
              ],
            ),
          ),
        )
    );
  }
}
