import 'EmployeeInterface.dart';

class Employee implements Person, Profession {
  @override
  int age;

  @override
  String name;

  @override
  var personAge;

  @override
  var personName;

  @override
  var personProfession;

  @override
  var personSalary;

  @override
  String prof;

  @override
  int salary;

}