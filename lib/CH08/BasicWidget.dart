import 'package:flutter/material.dart';

void main() => runApp(MyBasicWidgetApp());

class MyBasicWidgetApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        title: "Starter Template",
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        home: WidgetHome()
    );
  }
}

class WidgetHome extends StatefulWidget {
  @override
  _WidgetHomeState createState() => _WidgetHomeState();
}

class _WidgetHomeState extends State<WidgetHome>{
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Home'),
          leading: IconButton(
            icon: Icon(Icons.apps),
            onPressed: () {},
          ),
          actions: <Widget>[
            IconButton(
              icon: Icon(Icons.search),
              onPressed: () {},
            ),
            IconButton(
              icon: Icon(Icons.more_vert),
              onPressed: () {},
            )
          ],
          flexibleSpace: SafeArea(
            child: Icon(
              Icons.camera,
              size: 75.0,
              color: Colors.white,
            ),
          ),
          bottom: PreferredSize(
          preferredSize: Size.fromHeight(75.0),
          child: Container(
            child: Center(child: Text("Bottom")),
            color: Colors.lightBlue.shade100,
            height: 75.0,
            width: double.infinity,
          ),
        ),
        ),
        body: Padding(
          padding: EdgeInsets.all(16.0),
          child: SafeArea(
            child: SingleChildScrollView(
              child: Column(
                children: <Widget>[
                  const ContainerWithBoxDecorationWidget(),
                  const ColumnWidget(),
                  const RowWidget(),
                  const ColumnAndRowNestingWidget(),
                  const FlatRaisedIconButtonWidget()
                ],
              ),
            ),
          ),
        ),
      floatingActionButton: FloatingActionButton(
        onPressed: (){},
        child: Icon(Icons.play_arrow),
        backgroundColor: Colors.lightBlue.shade100,
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.endDocked,
      bottomNavigationBar: BottomAppBar(
        color: Colors.lightBlue.shade100,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            Icon(Icons.pause),
            Icon(Icons.stop),
            Icon(Icons.access_time),
          ],
        ),
      ),
    );
  }

}

class ContainerWithBoxDecorationWidget extends StatelessWidget{
  const ContainerWithBoxDecorationWidget({Key key}): super(key: key);
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
      height: 100,
      child: Center(
        child: RichText(
          text: TextSpan(
            text: "Flutter World ",
            style: TextStyle(
              fontSize: 24.0,
              fontStyle: FontStyle.italic,
              fontWeight: FontWeight.normal,
              color: Colors.purple,
              decorationColor: Colors.deepPurpleAccent,
              decorationStyle: TextDecorationStyle.dotted,
            ),
            children: <TextSpan>[
              TextSpan(
                text: "For "
              ),
              TextSpan(
                text: "Mobile",
                style: TextStyle(
                  color:  Colors.deepOrange,
                  fontWeight: FontWeight.bold,
                )
              )
            ]
          ),
        ),
      ),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.only(
          bottomLeft: Radius.circular(100.0),
          bottomRight: Radius.circular(10.0),
        ),
        gradient: LinearGradient(
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter,
          colors: [
            Colors.white,
            Colors.lightBlue,
          ]
      )
      ),
    );
  }

}

class ColumnWidget extends StatelessWidget {
  const ColumnWidget ({Key key}): super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      mainAxisSize: MainAxisSize.max,
      children: <Widget>[
        Text('Column 1'),
        Divider(),
        Text('Column 2'),
        Divider(),
        Text('Column 3'),
        Divider(),
      ],
    );
  }

}

class RowWidget extends StatelessWidget {
  const RowWidget ({Key key}): super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      mainAxisSize: MainAxisSize.max,
      children: <Widget>[
        Text('Row 1'),
        Text('Row 2'),
        Text('Row 3'),
      ],
    );
  }

}

class ColumnAndRowNestingWidget extends StatelessWidget {
  const ColumnAndRowNestingWidget ({Key key}): super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      mainAxisSize: MainAxisSize.max,
      children: <Widget>[
        Divider(),
        Text('Col n Row Nesting 1'),
        Text('Col n Row Nesting 2'),
        Text('Col n Row Nesting 3'),
        Divider(),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            Text("Row Nesting 1"),
            Text("Row Nesting 2"),
            Text("Row Nesting 3"),
          ],
        )
      ],
    );
  }

}

class FlatRaisedIconButtonWidget extends StatelessWidget {
  const FlatRaisedIconButtonWidget ({Key key}): super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      mainAxisSize: MainAxisSize.max,
      children: <Widget>[
        Row(
          children: <Widget>[
            FlatButton(
                onPressed: (){},
                child: Text("Flag"),
            ),
            FlatButton(
              onPressed: () {},
              color: Colors.lightBlue,
              child: Icon(
                Icons.flag,
                color: Colors.white,
              ),
            )
          ],
        ),
        Row(
          children: <Widget>[
            RaisedButton(
              onPressed: (){},
              child: Text('Save'),
            ),
            RaisedButton(
              onPressed: (){},
              child: Icon(Icons.save),
              color: Colors.lightBlue,
            )
          ],
        ),
        Row(
          children: <Widget>[
            IconButton(icon: Icon(Icons.flight), onPressed: (){}),
            IconButton(
              icon: Icon(Icons.flight),
              onPressed: (){},
              iconSize: 42.0,
              color: Colors.lightBlue,
              tooltip: 'Flight',
            )
          ],
        )
      ],
    );
  }

}