void main(){
  // Declaring variables to hold numbers
   int num1 = 30;
   int num2 = 20;
   double num3;
   bool result;

   // Assignment operator test num2 is now 50
  num2 += num1;
  print("Now Num2 is -> $num2");

  // Arithmetic operation with division
  //
  num3 = num1 / num2;
  print("Num1 divided by Num2 is-> $num3 ");

  result = (num1 >= num2);
  print("Is Num1 greater than Num2? $result");

  // Bitwise operation
  num1 = num1<<2;
  print("Now Num1 is-> $num1");

   num1 = num1 - num2;
   print("Num1 is substract by Num2 is-> $num1 ");

   num3 *= num2;
   print("Num3 multiplied by num2 is -> $num3");
}
