import 'dart:io';

void main(){
  List<String> month = new List();
  month = ["JANUARY", "FEBRUARY", "APRIL", "MAY", "JUNE"];

  month.add("JULY");
  month.insert(2, "MARCH");
  month.addAll(["NOVEMBER", "DECEMBER"]);
  print(month);
  month.insertAll(7, ["AUGUST", "SEPTEMBER", "OCTOBER"]);
  print(month);
  String badMonth = ( stdin.readLineSync());
  month.remove(badMonth);
  print(month);
  stdout.write("Which period do you like the least ?\n");
  stdout.write("Starting from the month (0 = January) : ? ->");
  int startRange = int.parse( stdin.readLineSync());
  stdout.write("To : ? ->");
  int endRange = int.parse( stdin.readLineSync());
  month.removeRange(startRange, endRange);
  print(month);
}