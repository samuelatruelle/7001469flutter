import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: "Starter Template",
      theme: ThemeData(
        primarySwatch: Colors.lightGreen,
      ),
      home: Home(),
    );
  }
}

class Home extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "Template",
      theme: ThemeData(
        primarySwatch: Colors.lightGreen,
      ),
      home: FontWidget(),
    );
  }
}

class FontWidget extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _FontWidgetState();
}

class _FontWidgetState extends State<FontWidget> {
  final GlobalKey<FormState> _formStateKey = GlobalKey<FormState>();
  FontInfo _font = new FontInfo(inputString: "Hello World");

  List<double> _fontSize = new List();
  List<String> _fontFamily = new List();

  bool _underlineValue = false;
  bool _weightValue = false;
  bool _styleValue = false;
  int _radioGroupValue;

  void initState() {
    super.initState();
    _fontSize..add(45.0)..add(30.0)..add(15.0);
    _fontFamily..add("Serif")..add("Normal");
    _radioGroupValue = 0;
  }

  String _validateTextRequired(String value) {
    return value.isEmpty ? 'Text Required' : null;
  }

  void _changeFont() {
    setState(() {
      if (_formStateKey.currentState.validate()) {
        _formStateKey.currentState.save();
      }
    });
  }

  Widget _radioWithLabelWidget(int val, String lable) {
    return Row(
      children: <Widget>[
        Radio(
          value: val,
          groupValue: _radioGroupValue,
          onChanged: (index) {
            setState(() {
              _radioGroupValue = index;
              _font.size = _fontSize[index];
            });
          },
        ),
        Text(lable),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text("Font"),
      ),
      body: SingleChildScrollView(
        child: SafeArea(
            child: Form(
                key: _formStateKey,
                autovalidate: true,
                onChanged: _changeFont,
                child: Padding(
                    padding: EdgeInsets.all(16.0),
                    child: Column(children: <Widget>[
                      Card(
                        color: Colors.lightGreen.shade200,
                        child: Padding(
                          padding: EdgeInsets.all(25.0),
                          child: Text(_font.inputString,
                              style: TextStyle(
                                fontSize: _font.size,
                                fontFamily: _font.family,
                                  decoration: _underlineValue == true
                                      ? TextDecoration.underline : TextDecoration.none,
                                  fontWeight: _weightValue == true
                                      ? FontWeight.bold  : FontWeight.normal,
                                  fontStyle: _styleValue == true
                                      ? FontStyle.italic : FontStyle.normal,
                              )),
                        ),
                      ),
                      TextFormField(
                        decoration: InputDecoration(
                          hintText: 'Hello World!',
                          labelText: 'Text',
                        ),
                        validator: (value) => _validateTextRequired(value),
                        onSaved: (value) => _font.inputString = value,
                        maxLength: 12,
                      ),
                      Row(
                        children: <Widget>[
                          _radioWithLabelWidget(0, "Big"),
                          _radioWithLabelWidget(1, "Medium"),
                          _radioWithLabelWidget(2, "Small"),
                        ],
                      ),
                      DropdownButton<String>(
                        value: _fontFamily[0],
                        items: _fontFamily.map((String value) {
                          return new DropdownMenuItem<String>(
                            value: value,
                            child: new Text(value),
                          );
                        }).toList(),
                        onChanged: (value) {
                          _font.family = value;
                          _changeFont();
                        },
                      ),
                      Column(
                        children: <Widget>[
                          CheckboxListTile(
                            title: Text("Underline"),
                            value: _underlineValue,
                            onChanged: (val) {
                              _underlineValue = val;
                              _changeFont();
                            },
                            controlAffinity: ListTileControlAffinity.leading,
                          ),
                          CheckboxListTile(
                            title: Text("Bold"),
                            value: _weightValue,
                            onChanged: (val) {
                              _weightValue = val;
                              _changeFont();
                            },
                            controlAffinity: ListTileControlAffinity.leading,
                          ),
                          CheckboxListTile(
                            title: Text("Italic"),
                            value: _styleValue,
                            onChanged: (val) {
                              _styleValue = val;
                              _changeFont();
                            },
                            controlAffinity: ListTileControlAffinity.leading,
                          ),
                        ],
                      )
                    ])))),
      ),
    );
    ;
  }
}

class FontInfo {
  String inputString;

  String family;
  double size;

  FontInfo({
    @required this.inputString,
    this.family = "Serif",
    this.size = 50,
  });
}
