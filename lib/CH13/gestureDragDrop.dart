import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: "GDD",
      theme: ThemeData(
        primarySwatch: Colors.lightGreen,
      ),
      home: GestureDetectorPage(),
    );
  }
}

class GestureDetectorPage extends StatefulWidget {
  @override
  _GestureDetectorPageState createState() => _GestureDetectorPageState();
}

class _GestureDetectorPageState extends State<GestureDetectorPage> {
  String _gestureDetected = "";
  Color _paintedColor;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Gesture Detection"),
        ),
        body: SafeArea(
          child: SingleChildScrollView(
            child: Column(
              children: <Widget>[
                _buildGestureDetector(),
                Divider(
                  color: Colors.black,
                ),
                _buildDraggable(),
                Divider(
                  color: Colors.black,
                ),
                _buildDragTarget(),
              ],
            ),
          ),
        ));
  }

  GestureDetector _buildGestureDetector() {
    return GestureDetector(
      onTap: () {
        print("On Tap");
        _displayGestureDetected("On Tap");
      },
      onDoubleTap: () {
        print("On DoubleTap");
        _displayGestureDetected("On DoubleTap");
      },
      onLongPress: () {
        print("On LongPress");
        _displayGestureDetected("On LongPress");
      },
      /*onPanUpdate: (DragUpdateDetails details) {
        print("On PanUpdate");
        _displayGestureDetected("On PanUpdate");
      },*/
      onVerticalDragUpdate: ((DragUpdateDetails details) {
        print('onVerticalDragUpdate: $details');
        _displayGestureDetected('onVerticalDragUpdate:\n$details');
      }),
      onHorizontalDragUpdate: (DragUpdateDetails details) {
        print('onHorizontalDragUpdate: $details');
        _displayGestureDetected('onHorizontalDragUpdate:\n$details');
      },
      child: Container(
        color: Colors.lightGreen.shade100,
        width: double.infinity,
        padding: EdgeInsets.all(24.0),
        child: Column(
          children: <Widget>[
            Icon(
              Icons.access_alarm,
              size: 98.0,
            ),
            Text(
              '$_gestureDetected',
              style: TextStyle(fontSize: 30),
            )
          ],
        ),
      ),
    );
  }

  void _displayGestureDetected(String gesture) {
    setState(() {
      _gestureDetected = gesture;
    });
  }

  Draggable<int> _buildDraggable() {
    return Draggable(
      child: Column(
        children: <Widget>[
          Icon(
            Icons.palette,
            color: Colors.grey,
            size: 48.0,
          ),
          Text("Drag me below to change color")
        ],
      ),
      feedback: Icon(
        Icons.brush,
        color: Colors.orange,
        size: 48.0,
      ),
      childWhenDragging: Icon(
        Icons.palette,
        color: Colors.deepOrange,
        size: 48.0,
      ),
      data: Colors.deepOrange.value,
    );
  }

  DragTarget<int> _buildDragTarget() {
    return DragTarget<int>(
      onAccept: (colorValue) {
        _paintedColor = Color(colorValue);
      },
      builder: (BuildContext context, List<dynamic> acceptedData,
              List<dynamic> rejectedData) =>
          acceptedData.isEmpty
              ? Text(
                  'Drag To and see color change',
                  style: TextStyle(color: _paintedColor),
                )
              : Text(
                  'Painting Color: $acceptedData',
                  style: TextStyle(
                    color: Color(acceptedData[0]),
                    fontWeight: FontWeight.bold,
                  ),
                ),
    );
  }
}
