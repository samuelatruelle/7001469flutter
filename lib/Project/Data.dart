import 'package:flappy_search_bar/flappy_search_bar.dart';
import 'package:flutter/material.dart';
import 'package:sqflite/sqflite.dart';
import 'dart:io' as io;
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: "Starter Template",
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: Home(),
    );
  }
}

class Home extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        primarySwatch: Colors.indigo,
      ),
      home: StudentPage(),
    );
  }
}

class StudentPage extends StatefulWidget {
  @override
  _StudentPageState createState() => _StudentPageState();
}

class _StudentPageState extends State<StudentPage> {
  final GlobalKey<FormState> _formStateKey = GlobalKey<FormState>();
  final GlobalKey<FormState> _formStateKeyAge = GlobalKey<FormState>();
  Future<List<Student>> students;
  int studentIdForUpdate;
  String _studentName;
  String _studentAge;
  bool isUpdate = false;
  DBHelper dbHelper;
  final _studentNameController = TextEditingController();
  final _studentAgeController = TextEditingController();
  String search;

  @override
  void initState() {
    super.initState();
    dbHelper = DBHelper();
    refreshStudentList();
  }

  refreshStudentList() {
    setState(() {
      students = dbHelper.getStudents();
    });
  }

  void saveSearch(value) {
    search = value;
  }

  /*List<Student> searchedStudent(value) {
    for (int i = 0; i < students. ;i++)

      return students.data;
  }*/

  SingleChildScrollView generateList(List<Student> students) {
    List<Student> studentBis = new List();

    students.forEach((student) {
      if (search == null || search == "" || student.name.contains(search)) {
        studentBis.add(student);
      }
    });
    return SingleChildScrollView(
      scrollDirection: Axis.vertical,
      child: SizedBox(
        width: double.infinity,
        child: DataTable(
          columns: [
            DataColumn(label: Text('NAME')),
            DataColumn(label: Text('AGE')),
            DataColumn(
              label: Text('DELETE'),
            )
          ],
          rows: studentBis.map((student) {
            return DataRow(
              cells: [
                DataCell(
                  Text(student.name ?? 'No name'),
                  onTap: () {
                    setState(() {
                      isUpdate = true;
                      studentIdForUpdate = student.id;
                    });
                    _studentNameController.text = student.name;
                  },
                ),
                DataCell(
                  Text(student.age ?? '?'),
                  onTap: () {
                    setState(() {
                      isUpdate = true;
                      studentIdForUpdate = student.id;
                    });
                    _studentAgeController.text = student.age;
                  },
                ),
                DataCell(
                  IconButton(
                    icon: Icon(Icons.delete),
                    onPressed: () {
                      dbHelper.delete(student.id);
                      refreshStudentList();
                    },
                  ),
                )
              ],
            );
          }).toList(),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
        appBar: AppBar(
          title: Text('Student Saver'),
        ),
        body: Column(children: <Widget>[
          Form(
            key: _formStateKey,
            autovalidate: true,
            child: Padding(
              padding: EdgeInsets.only(left: 10, right: 10, bottom: 10),
              child: TextFormField(
                validator: (value) =>
                value.isEmpty ? 'Please Enter Student Name' : null,
                onSaved: (value) {
                  _studentName = value;
                },
                controller: _studentNameController,
                decoration: InputDecoration(
                    labelText: "Student Name",
                    labelStyle: TextStyle(
                      color: Colors.indigo,
                    )),
              ),
            ),
          ),
          Form(
            key: _formStateKeyAge,
            autovalidate: true,
            child: Padding(
              padding: EdgeInsets.only(left: 10, right: 10, bottom: 10),
              child: TextFormField(
                validator: (value) =>
                value.isEmpty ? 'Please Enter an Age' : null,
                onSaved: (value) {
                  _studentAge = value;
                },
                controller: _studentAgeController,
                decoration: InputDecoration(
                    labelText: "Student Age",
                    labelStyle: TextStyle(
                      color: Colors.indigo,
                    )),
              ),
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              RaisedButton(
                color: Colors.indigo,
                child: Text(
                  (isUpdate ? 'UPDATE' : 'ADD'),
                  style: TextStyle(color: Colors.white),
                ),
                onPressed: () {
                  if (isUpdate) {
                    if (_formStateKey.currentState.validate() &&
                        _formStateKeyAge.currentState.validate()) {
                      _formStateKey.currentState.save();
                      _formStateKeyAge.currentState.save();
                      dbHelper
                          .update(Student(
                          studentIdForUpdate, _studentName, _studentAge))
                          .then((data) {
                        setState(() {
                          isUpdate = false;
                        });
                      });
                    }
                  } else {
                    if (_formStateKey.currentState.validate() &&
                        _formStateKeyAge.currentState.validate()) {
                      _formStateKey.currentState.save();
                      _formStateKeyAge.currentState.save();
                      dbHelper.add(Student(null, _studentName, _studentAge));
                    }
                  }
                  _studentNameController.text = '';
                  _studentAgeController.text = '';
                  refreshStudentList();
                },
              ),
              Padding(
                padding: EdgeInsets.all(10),
              ),
              RaisedButton(
                color: Colors.deepPurpleAccent,
                child: Text(
                  ('SEARCH'),
                  style: TextStyle(color: Colors.white),
                ),
                onPressed: () {
                  if (isUpdate) {
                    if (_formStateKey.currentState.validate() &&
                        _formStateKeyAge.currentState.validate()) {
                      _formStateKey.currentState.save();
                      _formStateKeyAge.currentState.save();
                      dbHelper
                          .update(Student(
                          studentIdForUpdate, _studentName, _studentAge))
                          .then((data) {
                        setState(() {
                          isUpdate = false;
                        });
                      });
                    }
                  } else {
                    if (_formStateKey.currentState.validate() &&
                        _formStateKeyAge.currentState.validate()) {
                      _formStateKey.currentState.save();
                      _formStateKeyAge.currentState.save();
                      dbHelper.add(Student(null, _studentName, _studentAge));
                    }
                  }
                  _studentNameController.text = '';
                  _studentAgeController.text = '';
                  refreshStudentList();
                },
              ),
              Padding(
                padding: EdgeInsets.all(10),
              ),
              RaisedButton(
                color: Colors.amber,
                child: Text(
                  ("CANCEL"),
                  style: TextStyle(color: Colors.white),
                ),
                onPressed: () {
                  _studentNameController.text = '';
                  _studentAgeController.text = '';
                  setState(() {
                    //isUpdate = false;
                    studentIdForUpdate = null;
                  });
                },
              ),
            ],
          ),
          const Divider(
            height: 5.0,
          ),
          TextField(
              decoration: InputDecoration(
                border: OutlineInputBorder(),
                labelText: 'Searched Name',
              ),
              onSubmitted: (String value) async {
                await showDialog<void>(
                  context: context,
                  builder: (BuildContext context) {
                    saveSearch(value);

                    return Container();
                  },
                );
              }),
          const Divider(
            height: 5.0,
          ),
          Expanded(
            child: FutureBuilder(
              future: students,
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  return generateList(snapshot.data);
                }
                if (snapshot.data == null || snapshot.data.length == 0) {
                  return Text('No Data Found');
                }
                return CircularProgressIndicator();
              },
            ),
          ),
        ]));
  }
}

class Student {
  int id;
  String name;
  String age;

  Student(this.id, this.name, this.age);

  Map<String, dynamic> toMap() {
    var map = <String, dynamic>{
      'id': id,
      'name': name,
      'age': age,
    };
    return map;
  }

  Student.fromMap(Map<String, dynamic> map) {
    id = map['id'];
    name = map['name'];
    age = map['age'];
  }
}

class DBHelper {
  static Database _db;

  Future<Database> get db async {
    if (_db != null) {
      return _db;
    }
    _db = await initDatabase();
    return _db;
  }

  initDatabase() async {
    io.Directory documentDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentDirectory.path, 'studentage.db');
    var db = await openDatabase(path, version: 1, onCreate: _onCreate);
    return db;
  }

  _onCreate(Database db, int version) async {
    await db.execute(
        'CREATE TABLE student (id INTEGER PRIMARY KEY, name TEXT, age TEXT)');
  }

  Future<Student> add(Student student) async {
    var dbClient = await db;
    student.id = await dbClient.insert('student', student.toMap());
    return student;
  }

  Future<List<Student>> getStudents() async {
    var dbClient = await db;
    List<Map> maps =
    await dbClient.query('student', columns: ['id', 'name', 'age']);
    List<Student> students = [];
    if (maps.length > 0) {
      for (int i = 0; i < maps.length; i++) {
        students.add(Student.fromMap(maps[i]));
      }
    }
    return students;
  }

  Future<int> delete(int id) async {
    var dbClient = await db;
    return await dbClient.delete(
      'student',
      where: 'id = ?',
      whereArgs: [id],
    );
  }

  Future<int> update(Student student) async {
    var dbClient = await db;
    return await dbClient.update(
      'student',
      student.toMap(),
      where: 'id = ?',
      whereArgs: [student.id],
    );
  }

  Future close() async {
    var dbClient = await db;
    dbClient.close();
  }
}
