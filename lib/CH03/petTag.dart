import 'dart:io';

void main()
{
  String pet;   // "cat" or "dog"
  String spayed;


  // 'y' or 'n'
  // Get pet type and spaying information
  stdout.write("Enter the pet type (cat or dog): ");
  pet = stdin.readLineSync();
  stdout.write("Has the pet been spayed or neutered (y/n)? ");
  spayed = stdin.readLineSync();
  if (pet == "cat" && spayed == 'y')
      stdout.write("Fee is \$4.00 ");
  else if (pet == "cat")
      stdout.write("Fee is \$8.00 ");
  if (pet == "dog" && spayed == 'y')
      stdout.write("Fee is \$6.00 ");
  else if (pet == "dog")
      stdout.write("Fee is \$12.00 ");
  else if (pet != "dog" && pet != "cat")
    stdout.write("Only cats and dogs need pet tags. ");
}