import 'dart:math';

void main() {
  // TRIES> # of dart throws, hits> number of times square is hit
  int TRIES = 20000,
      hits = 0;
  double r, x, y;

  var random = Random.secure();
  for(int i = 0; i < TRIES; i++)
  {
    r = random.nextDouble();
    x = -1 + 2 * r; // x-coordinate is calculated
    r = random.nextDouble();
    y = -1 + 2 * r; // y-coordinate is calculated
    if (x * x + y * y <= 1)
      hits = hits + 1;
  }
  print("The PI estimate is=> ${4.0 * hits / TRIES}");
}
