import 'dart:io';
import 'Questions.dart';

void main(){
  Questions q = new Questions();
  q.setQuestionText("Who is the inventor of Dart?");
  q.setQuestionAns("Lars Bak".toUpperCase());

  q.display();
  stdout.write("Your answer is: ");
  String response = stdin.readLineSync();
  if (q.checkAnswer(response.toUpperCase()))
    print("You are right !");
  else
    print("You are wrong !");
}
